package com.adia.srs.bookingservice.consumerdrivencontract.asconsumer;

import com.adia.srs.bookingservice.domain.entities.Booking;
import com.adia.srs.bookingservice.domain.entities.CheckInStatus;
import com.adia.srs.bookingservice.domain.entities.Customer;
import com.adia.srs.bookingservice.domain.entities.Gender;
import com.adia.srs.bookingservice.domain.events.CheckInStatusChangedEvent;
import com.adia.srs.bookingservice.domain.repositories.BookingRepository;
import com.adia.srs.bookingservice.domain.repositories.CustomerRepository;
import com.adia.srs.bookingservice.gateway.MessagingGateway;
import com.adia.srs.bookingservice.service.CustomerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cloud.contract.stubrunner.StubTrigger;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.NONE)
@AutoConfigureStubRunner(ids = "com.adia.srs:portcheckinservice:+")
@ActiveProfiles(profiles = "testing")
public class MessagingTests {

    @Autowired
    StubTrigger stubTrigger;

    @SpyBean
    private MessagingGateway messagingGateway;

    @Autowired
    private CustomerService customerService;

    @MockBean
    private BookingRepository bookingRepository;

    @MockBean
    private CustomerRepository customerRepository;

    private Customer customer;
    private Booking bookingA;
    private Booking bookingB;

    @Before
    public void setup() throws Exception {
        customer = new Customer("Stefan", "Sarstedt", Gender.MALE, "stefan.sarstedt@haw-hamburg.de");
        customer.setId(1L);
        bookingA = new Booking("Aida");
        bookingA.setId(1L);
        customer.addBooking(bookingA);
        bookingB = new Booking("Mein Schiff 400");
        bookingB.setId(2L);
        bookingB.updateCheckInStatus(CheckInStatus.CHECKED_IN);
        customer.addBooking(bookingB);

        given(customerRepository.findById(any(Long.class))).willReturn(Optional.of(customer));
        given(customerRepository.save(any(Customer.class))).willReturn(customer);

        given(bookingRepository.findById(1L)).willReturn(Optional.of(bookingA));
        given(bookingRepository.findById(2L)).willReturn(Optional.of(bookingB));
        given(bookingRepository.save(any(Booking.class))).willReturn(bookingA);
    }

    @Test
    public void testCheckInCompliesToContract() throws Exception {

        stubTrigger.trigger("testCheckIn");

        ArgumentCaptor<CheckInStatusChangedEvent> argument = ArgumentCaptor.forClass(CheckInStatusChangedEvent.class);
        verify(messagingGateway).handle(argument.capture());
        assertThat(argument.getValue().getBookingId()).isEqualTo(1L);
        assertThat(argument.getValue().getNewStatus()).isEqualTo(CheckInStatus.CHECKED_IN);
    }

    @Test
    public void testCheckOutCompliesToContract() throws Exception {

        stubTrigger.trigger("testCheckOut");

        ArgumentCaptor<CheckInStatusChangedEvent> argument = ArgumentCaptor.forClass(CheckInStatusChangedEvent.class);
        verify(messagingGateway).handle(argument.capture());
        assertThat(argument.getValue().getBookingId()).isEqualTo(2L);
        assertThat(argument.getValue().getNewStatus()).isEqualTo(CheckInStatus.CHECKED_OUT);
    }
}

