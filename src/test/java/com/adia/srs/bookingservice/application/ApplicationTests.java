package com.adia.srs.bookingservice.application;

import com.adia.srs.bookingservice.Application;
import com.adia.srs.bookingservice.domain.dtos.BookingDTO;
import com.adia.srs.bookingservice.domain.dtos.CustomerCreateDTO;
import com.adia.srs.bookingservice.domain.dtos.CustomerUpdateDTO;
import com.adia.srs.bookingservice.domain.entities.Customer;
import com.adia.srs.bookingservice.domain.entities.Gender;
import com.adia.srs.bookingservice.domain.events.BookingCreatedEvent;
import com.adia.srs.bookingservice.domain.repositories.CustomerRepository;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.stream.test.binder.MessageCollector;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.text.MatchesPattern.matchesPattern;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureJsonTesters
@ActiveProfiles(profiles = "testing")
public class ApplicationTests {

    private final Log log = LogFactory.getLog(getClass());

    @LocalServerPort
    private int port;

    @Autowired
    private CustomerRepository customerRepository;

    private Customer customer;

    @Autowired
    private Source source;

    @Autowired
    private MessageCollector messageCollector;

    @Autowired
    private JacksonTester<BookingCreatedEvent> json;

    @Before
    public void setUp() {
        customer = this.customerRepository.save(new Customer("Stefan", "Sarstedt", Gender.MALE, "stefan.sarstedt@haw-hamburg.de"));

        RestAssured.port = port;
        RestAssured.basePath = "/v1";

        messageCollector.forChannel(source.output()).clear();
    }

    // -------------------------------------------------------------------------------------------------------------------
    // für JSONPath siehe http://goessner.net/articles/JsonPath/ und den Tester unter https://jsonpath.curiousconcept.com/
    // -------------------------------------------------------------------------------------------------------------------
    @Test
    public void getAllCustomersSuccess() {
        //@formatter:off
        given().
                // add this here to log request --> log().all().
        when().
                get("/customers").
        then().
                // add this here to log response --> log().all().
                statusCode(HttpStatus.OK.value()).
                body("lastName", hasItems("Sarstedt"));
        //@formatter:on
    }

    @Test
    public void getCustomerSuccess() {
        //@formatter:off
        given().
        when().
                get("/customers/{id}", customer.getId()).
        then().
                statusCode(HttpStatus.OK.value()).
                body("lastName", equalTo("Sarstedt"));
        //@formatter:on
    }

    @Test
    public void getCustomerFailBecauseOfCustomerNotFound() {
        //@formatter:off
        given().
        when().
                get("/customers/{id}", Integer.MAX_VALUE).
        then().
                statusCode(HttpStatus.NOT_FOUND.value());
        //@formatter:on
    }

    @Test
    public void addCustomerSuccess() {
        //@formatter:off
        given().
                contentType(ContentType.JSON).
                body(new CustomerCreateDTO("Hui", "Boo", Gender.OTHER, "hui@boo.de")).
                post("/customers").
        then().
                statusCode(HttpStatus.CREATED.value()).
                body(matchesPattern("^[1-9]\\d*$")); // match positive number (i.e., if id of new customer was returned);
        //@formatter:on
    }

    @Test
    public void addCustomerFailBecauseOfInvalidDTO() {
        //@formatter:off
        given().
                contentType(ContentType.JSON).
                body(new CustomerCreateDTO("Hui", "Boo", Gender.OTHER, "invalidEmail")).
                post("/customers").
        then().
                statusCode(HttpStatus.BAD_REQUEST.value());
        //@formatter:on
    }

    @Test
    public void deleteCustomerSuccess() {

        addCustomerSuccess();

        //@formatter:off
        given().
                delete("/customers/{id}", customer.getId()).
        then().
                statusCode(HttpStatus.OK.value());
        //@formatter:on
    }

    @Test
    public void deleteCustomerFailBecauseOfCustomerNotFound() {
        //@formatter:off
        given().
                delete("/customers/{id}", Integer.MAX_VALUE).
        then().
                statusCode(HttpStatus.NOT_FOUND.value());
        //@formatter:on
    }

    @Test
    public void addCustomerFailBecauseOfValidationFailure() {
        //@formatter:off
        given().
                contentType(ContentType.JSON).
                body(new CustomerCreateDTO("", "", Gender.OTHER, "invalidmail")).
                post("/customers").
        then().
                statusCode(HttpStatus.BAD_REQUEST.value());
        //@formatter:on
    }

    @Test
    public void updateCustomerSuccess() {
        //@formatter:off
        given().
                contentType(ContentType.JSON).
                body(new CustomerUpdateDTO(customer.getId(), "hui@boo.de")).
        when().
                put("/customers").
        then().
                statusCode(HttpStatus.OK.value());
        //@formatter:on
    }

    @Test
    public void addBookingToCustomerSuccess() throws IOException {
        //@formatter:off
        int bookingId = given().
                contentType(ContentType.JSON).
                body(new BookingDTO("Mein Schiff 42")).
        when().
                post("/customers/{id}/bookings", customer.getId()).
        then().
                statusCode(HttpStatus.CREATED.value()).
                body(matchesPattern("^[1-9]\\d*$")). // match positive number (i.e., if id of new booking was returned)
        extract().
                body().as(Integer.class);
        //@formatter:on

        // check if BookingCreatedEvent was sent
        Message message = messageCollector.forChannel(source.output())
                .poll();
        assertThat(message).isNotNull();
        MessageHeaders headers = message.getHeaders();
        assertThat(headers.containsKey("type")).isTrue();
        assertThat(headers.get("type")).isEqualTo(BookingCreatedEvent.class.getSimpleName());
        Object payload = message.getPayload();
        assertThat(this.json.parse(payload.toString()).getObject().getBookingId()).isEqualTo(bookingId);
    }

    @Test
    public void getAllBookingsSuccess() {
        //@formatter:off
        int bookingId = given().
                contentType(ContentType.JSON).
                body(new BookingDTO("Mein Schiff 42")).
        when().
                post("/customers/{id}/bookings", customer.getId()).
        then().
                statusCode(HttpStatus.CREATED.value()).
                body(matchesPattern("^[1-9]\\d*$")).
        extract().
                body().as(Integer.class);

        given().
        when().
                get("/bookings").
        then().
                log().all().
                statusCode(HttpStatus.OK.value()).
                body("id", hasItems(bookingId));
        //@formatter:on
    }

    @Test
    public void addBookingToCustomerFailBecauseOfCustomerNotFound() {
        //@formatter:off
        given().
                contentType(ContentType.JSON).
                body(new BookingDTO("some ship")).
        when().
                post("/customers/{id}/bookings", Integer.MAX_VALUE).
        then().
                statusCode(HttpStatus.NOT_FOUND.value());
        //@formatter:on
    }
}