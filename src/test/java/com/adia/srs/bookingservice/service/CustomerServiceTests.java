package com.adia.srs.bookingservice.service;

import com.adia.srs.bookingservice.Application;
import com.adia.srs.bookingservice.domain.dtos.CreditCheckDTO;
import com.adia.srs.bookingservice.domain.entities.Customer;
import com.adia.srs.bookingservice.domain.entities.Gender;
import com.adia.srs.bookingservice.domain.repositories.CustomerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles(profiles = "testing")
public class CustomerServiceTests {

    @Autowired
    private CustomerService customerService;

    @MockBean
    private CustomerRepository customerRepository;

    @MockBean
    private PaymentServiceAdapter paymentServiceAdapter;

    @Test
    public void serviceTestGetCustomers() {
        given(this.customerRepository.findAll()).willReturn(
                Collections.singletonList(new Customer("Jane", "Doe", Gender.FEMALE, "jane.doe@mail.com")));

        List<Customer> actual = customerService.getCustomers();
        assertThat(actual).size().isEqualTo(1);
        assertThat(actual.get(0).getFirstName()).isEqualTo("Jane");
    }

    @Test
    public void serviceTestCheckCredit() {
        given(this.paymentServiceAdapter.checkCredit(42)).willReturn(new CreditCheckDTO(true));

        boolean creditOk = customerService.checkCredit(42);
        assertThat(creditOk).isTrue();
    }
}