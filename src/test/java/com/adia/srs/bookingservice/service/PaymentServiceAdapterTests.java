package com.adia.srs.bookingservice.service;

import com.adia.srs.bookingservice.domain.dtos.CreditCheckDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@RestClientTest({PaymentServiceAdapter.class})
@AutoConfigureWebClient(registerRestTemplate = true)
@ActiveProfiles(profiles = "testing")
public class PaymentServiceAdapterTests {

    @Autowired
    private PaymentServiceAdapter paymentServiceAdapter;

    @Value("${PAYMENTSERVICE_URL:http://paymentservice/checkCredit?customerId=%d}")
    private String paymentServiceURL;

    @Autowired
    private MockRestServiceServer server;

    @Test
    public void serviceTestCheckCredit() {
        this.server.expect(
                requestTo(String.format(paymentServiceURL, 42))).
                andRespond(withSuccess("{ \"creditOk\":true }", MediaType.APPLICATION_JSON));

        CreditCheckDTO creditCheckDTO = paymentServiceAdapter.checkCredit(42);
        assertThat(creditCheckDTO.isCreditOk()).isTrue();
    }
}