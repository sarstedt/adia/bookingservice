package com.adia.srs.bookingservice.service;

import com.adia.srs.bookingservice.domain.dtos.BookingDTO;
import com.adia.srs.bookingservice.domain.entities.Booking;
import com.adia.srs.bookingservice.domain.entities.BookingStatus;
import com.adia.srs.bookingservice.domain.entities.Customer;
import com.adia.srs.bookingservice.domain.events.BookingCreatedEvent;
import com.adia.srs.bookingservice.domain.events.BookingStatusChangedEvent;
import com.adia.srs.bookingservice.domain.events.CheckInStatusChangedEvent;
import com.adia.srs.bookingservice.domain.repositories.BookingRepository;
import com.adia.srs.bookingservice.domain.repositories.CustomerRepository;
import com.adia.srs.bookingservice.exceptions.BookingNotFoundException;
import com.adia.srs.bookingservice.exceptions.CustomerNotFoundException;
import com.adia.srs.bookingservice.gateway.MessagingGateway;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final BookingRepository bookingRepository;
    private final PaymentServiceAdapter paymentServiceAdapter;
    private final MessagingGateway messagingGateway;

    private final Log log = LogFactory.getLog(getClass());

    @Autowired
    public CustomerService(CustomerRepository customerRepository,
                           BookingRepository bookingRepository,
                           PaymentServiceAdapter paymentServiceAdapter,
                           @Lazy MessagingGateway messagingGateway) {  // cyclic dependency resolution; see http://www.baeldung.com/circular-dependencies-in-spring
        this.customerRepository = customerRepository;
        this.bookingRepository = bookingRepository;
        this.paymentServiceAdapter = paymentServiceAdapter;
        this.messagingGateway = messagingGateway;
    }

    public List<Customer> getCustomers() {
        return customerRepository.findAll();
    }

    public boolean checkCredit(long customerId) {
        return paymentServiceAdapter.checkCredit(customerId).isCreditOk();
    }

    public Booking addBooking(Long customerId, BookingDTO bookingDTO) throws CustomerNotFoundException {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerId);

        if (optionalCustomer.isPresent()) {
            Customer customer = optionalCustomer.get();

            Booking booking = bookingRepository.save(Booking.of(bookingDTO));

            customer.addBooking(booking);
            customerRepository.save(customer);

            messagingGateway.publish(new BookingCreatedEvent(booking.getId()));

            return booking;
        } else {
            throw new CustomerNotFoundException(customerId);
        }
    }

    public void updateBookingStatus(Booking booking, BookingStatus newStatus) {
        BookingStatusChangedEvent bookingStatusChangedEvent = new BookingStatusChangedEvent();
        bookingStatusChangedEvent.setBookingId(booking.getId());
        bookingStatusChangedEvent.setOldStatus(booking.getBookingStatus());
        bookingStatusChangedEvent.setNewStatus(newStatus);

        booking.updateBookingStatus(newStatus);
        bookingRepository.save(booking);

        messagingGateway.publish(bookingStatusChangedEvent);
    }

    public void handle(CheckInStatusChangedEvent checkInStatusChangedEvent) throws BookingNotFoundException {
        Optional<Booking> optionalBooking = bookingRepository.findById(checkInStatusChangedEvent.getBookingId());

        if (optionalBooking.isPresent()) {
            Booking booking = optionalBooking.get();

            booking.updateCheckInStatus(checkInStatusChangedEvent.getNewStatus());
            bookingRepository.save(booking);
        } else {
            throw new BookingNotFoundException(checkInStatusChangedEvent.getBookingId());
        }
    }
}
