package com.adia.srs.bookingservice.facade;

import com.adia.srs.bookingservice.domain.entities.Booking;
import com.adia.srs.bookingservice.domain.repositories.BookingRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/v1/bookings")
@Api(value = "/v1/bookings", tags = "Bookings")
public class BookingController {

    private final BookingRepository bookingRepository;

    private final Log log = LogFactory.getLog(getClass());

    @Autowired
    public BookingController(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    @ApiOperation(value = "Get booking by id", response = Booking.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved booking with given id"),
            @ApiResponse(code = 404, message = "Booking is not found")
    })
    @GetMapping(value = "/{id:[\\d]+}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getBookingsAvailable(@RequestParam("isAvailable") Boolean flag) {
        return "{  \"available\" : true }";
    }

    @ApiOperation(value = "Get all bookings", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved bookings"),
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Booking> getBookings() {

        return bookingRepository.findAll();
    }
}
