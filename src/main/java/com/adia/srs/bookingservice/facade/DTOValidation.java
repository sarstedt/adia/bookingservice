package com.adia.srs.bookingservice.facade;

import com.adia.srs.bookingservice.exceptions.EntityInvalidException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.CodeSignature;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Set;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DTOValidation {
}

@Aspect
@Component
class DTOValidationAspect {

    private Validator entityValidator = Validation.buildDefaultValidatorFactory().getValidator();

    @Before("@annotation(com.adia.srs.bookingservice.facade.DTOValidation) && args(dtoArgument)")
    public void performEntityValidation(JoinPoint joinPoint, Object dtoArgument) throws EntityInvalidException {

        CodeSignature codeSignature = (CodeSignature) joinPoint.getSignature();
        Class[] parameterTypes = codeSignature.getParameterTypes();
        int i = 0;
        for (Class parameterType : parameterTypes) {
            if (parameterType.getName().toUpperCase().endsWith("DTO")) {

                Object param = joinPoint.getArgs()[i];

                Set<ConstraintViolation<Object>> violations = entityValidator.validate(param);
                if (!violations.isEmpty()) {
                    throw new EntityInvalidException(violations.toString());
                }
            }
            i++;
        }
    }
}