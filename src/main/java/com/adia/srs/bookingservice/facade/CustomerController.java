package com.adia.srs.bookingservice.facade;

import com.adia.srs.bookingservice.domain.dtos.BookingDTO;
import com.adia.srs.bookingservice.domain.dtos.CustomerCreateDTO;
import com.adia.srs.bookingservice.domain.dtos.CustomerUpdateDTO;
import com.adia.srs.bookingservice.domain.entities.Booking;
import com.adia.srs.bookingservice.domain.entities.Customer;
import com.adia.srs.bookingservice.domain.repositories.CustomerRepository;
import com.adia.srs.bookingservice.exceptions.CustomerNotFoundException;
import com.adia.srs.bookingservice.exceptions.EntityInvalidException;
import com.adia.srs.bookingservice.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/v1/customers")
@Api(value = "/v1/customers", tags = "Customers")
public class CustomerController {

    private final CustomerService customerService;

    private final CustomerRepository customerRepository;

    private final Log log = LogFactory.getLog(getClass());

    @Autowired
    public CustomerController(CustomerService customerService, CustomerRepository customerRepository) {
        this.customerService = customerService;
        this.customerRepository = customerRepository;
    }

    @ApiOperation(value = "Get all customers", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved bookings"),
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Customer> getCustomers() {

        return customerRepository.findAll();
    }

    @ApiOperation(value = "Get a customer by Id", response = Customer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved customer"),
            @ApiResponse(code = 404, message = "Customer is not found")
    })
    @GetMapping(value = "/{id:[\\d]+}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Customer getCustomer(@PathVariable("id") Long customerId) throws CustomerNotFoundException {

        return customerRepository
                .findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException(customerId));
    }

    @DeleteMapping("/{id:[\\d]+}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteCustomer(@PathVariable("id") Long customerId) throws CustomerNotFoundException {

        Customer customer = customerRepository
                .findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException(customerId));

        customerRepository.delete(customer);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @DTOValidation
    public Long addCustomer(@RequestBody CustomerCreateDTO customerCreateDTO) throws EntityInvalidException {

        return customerRepository.save(Customer.of(customerCreateDTO)).getId();
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @DTOValidation
    public void updateCustomer(@RequestBody CustomerUpdateDTO customerUpdateDTO) throws CustomerNotFoundException {

        Customer customerToUpdate = customerRepository
                .findById(customerUpdateDTO.getId())
                .orElseThrow(() -> new CustomerNotFoundException(customerUpdateDTO.getId()));

        customerRepository.save(customerToUpdate);
    }

    @PostMapping(value = "/{id}/bookings")
    @DTOValidation
    public ResponseEntity addBooking(@PathVariable("id") Long customerId, @RequestBody BookingDTO bookingDTO) {
        try {
            Booking booking = customerService.addBooking(customerId, bookingDTO);
            return new ResponseEntity(booking.getId(), HttpStatus.CREATED);
        } catch (CustomerNotFoundException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
