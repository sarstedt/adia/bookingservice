package com.adia.srs.bookingservice.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Data
@ResponseStatus(HttpStatus.NOT_FOUND)
public class CustomerNotFoundException extends Exception {

    private final Long customerId;

    public CustomerNotFoundException(Long customerId) {
        super("Could not find Customer '" + customerId + "'.");

        this.customerId = customerId;
    }
}