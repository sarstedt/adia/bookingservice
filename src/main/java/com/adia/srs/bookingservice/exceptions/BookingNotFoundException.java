package com.adia.srs.bookingservice.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Data
@ResponseStatus(HttpStatus.NOT_FOUND)
public class BookingNotFoundException extends Exception {

    private final Long bookingId;

    public BookingNotFoundException(Long bookingId) {
        super("Could not find Booking with bookingnumber '" + bookingId + "'.");

        this.bookingId = bookingId;
    }
}