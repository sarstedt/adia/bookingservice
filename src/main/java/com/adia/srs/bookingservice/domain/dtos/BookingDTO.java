package com.adia.srs.bookingservice.domain.dtos;

import lombok.*;

import javax.validation.constraints.Size;

@Data
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
public class BookingDTO {

    @Size(min = 1, max = 20)
    public String ferry;
}
