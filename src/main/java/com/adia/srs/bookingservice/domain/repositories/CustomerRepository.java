package com.adia.srs.bookingservice.domain.repositories;

import com.adia.srs.bookingservice.domain.entities.Booking;
import com.adia.srs.bookingservice.domain.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Optional<Customer> findByLastName(String lastName);

    Optional<Customer> findByBookingsContaining(Booking booking);
}
