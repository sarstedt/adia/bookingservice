package com.adia.srs.bookingservice.domain.events;

import com.adia.srs.bookingservice.domain.entities.CheckInStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CheckInStatusChangedEvent {

    private Long bookingId;

    private CheckInStatus newStatus;
}
