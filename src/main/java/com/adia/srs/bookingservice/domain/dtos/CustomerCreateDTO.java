package com.adia.srs.bookingservice.domain.dtos;

import com.adia.srs.bookingservice.domain.entities.Gender;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
public class CustomerCreateDTO {

    @Size(min = 1, max = 100)
    private String firstName;

    @Size(min = 1, max = 100)
    private String lastName;

    private Gender gender;

    @Email
    private String email;
}
