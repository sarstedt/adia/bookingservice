package com.adia.srs.bookingservice.domain.dtos;

import lombok.*;

import javax.validation.constraints.Email;

@Data
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
public class CustomerUpdateDTO {

    private Long id;

    @Email
    private String email;
}
